#-*- coding: utf-8 -*-

from kungfu.contrib.ops.shortcuts import *

################################################################################

@fqdn.route(r'^$', strategy='login')
def homepage(context):
    context.template = 'ops/views/homepage.html'

    return {'version': version}

################################################################################

from . import basis
from . import metalogy

################################################################################

urlpatterns = fqdn.urlpatters
