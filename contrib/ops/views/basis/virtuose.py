#-*- coding: utf-8 -*-

from kungfu.contrib.ops.shortcuts import *

################################################################################

@fqdn.route(r'^virt/?$', strategy='login')
def listing(context):
    context.template = 'data/views/virt/list.html'

    return {'version': version}

#*******************************************************************************

@fqdn.route(r'^virt/(?P<provider>.+)/(?P<owner>.+)/(?P<slug>.+)$', strategy='login')
def overview(context, provider, owner, slug):
    context.template = 'data/views/virt/view.html'

    return {'version': version}
