#-*- coding: utf-8 -*-

from kungfu.contrib.ops.shortcuts import *

################################################################################

@fqdn.route(r'^platforms/?$', strategy='login')
def listing(context):
    context.template = 'ops/rest/platform/list.html'

    return {'version': version}

#*******************************************************************************

@fqdn.route(r'^platforms/(?P<name>.+)/?$', strategy='login')
def overview(context, name):
    context.template = 'ops/rest/platform/view.html'

    return {
        'name': name,
    }
