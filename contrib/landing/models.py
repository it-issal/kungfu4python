#-*- coding: utf-8 -*-

from .utils import *

################################################################################

class TeamRole(models.Model):
    name       = models.CharField(max_length=128)

    def __str__(self):     return str(self.name)
    def __unicode__(self): return unicode(self.name)

#*******************************************************************************

class TeamMember(models.Model):
    identity   = models.OneToOneField(CustomUser, related_name='membership')

    is_founder = models.BooleanField(default=False)
    roles      = models.ManyToManyField(TeamRole, blank=True, related_name='profiles')

    joined_on  = models.DateTimeField(auto_now_add=True)

    def __str__(self):     return str(self.identity)
    def __unicode__(self): return unicode(self.identity)
