#-*- coding: utf-8 -*-

from myopla.shortcuts import *

from myopla.core.models import *
from myopla.crm.models import *

################################################################################

class HostNode(models.Model):
    name            = models.CharField(max_length=64)

    fqdn            = models.CharField(max_length=64, blank=True)

    ip_addr4        = models.GenericIPAddressField('IPv4', blank=True, null=True, default=None)
    ip_addr6        = models.GenericIPAddressField('IPv6', blank=True, null=True, default=None)

    api_login       = models.CharField(max_length=64, blank=True)
    api_passwd      = models.CharField(max_length=64, blank=True)

    owner           = models.ForeignKey(Profile, related_name='nodes', blank=True, null=True, default=None)
    project         = models.ForeignKey('ci.Project', related_name='nodes', blank=True, null=True, default=None)

    @property
    def authorized_keys(self):
        return '\n'.join([
            pki.pub_key
            for pki in self.ssh_keys.all()
        ])

    ########################################################################################

    hipchat_room_id = models.PositiveIntegerField(blank=True, null=True, default=None)

    @property
    def hipchat_room(self):
        if not self.hipchat_room_id:
            st = HipChat.create_room(
                name          = "@"+self.name,
                owner_user_id = settings.HIPCHAT_OWNER,
                public        = True,
            )

            if st:
                x = dir(st)

                self.hipchat_room_id = st['id']

        return HipChat.get_room_details(room_id=self.hipchat_room_id or settings.HIPCHAT_ROOM)

    def notify(self, msg, level='notice'):
        return HipChat.send_messages(
            room_id = self.hipchat_room_id or settings.HIPCHAT_ROOM,
            sender  = "@"+self.name,
            message = msg,
        )

    ########################################################################################

    trello_board_id = models.PositiveIntegerField(blank=True, null=True, default=None)

    @property
    def trello_board(self):
        if not self.trello_board_id:
            resp = Trello.boards.new(self.name)

            if resp:
                self.trello_board_id = resp['id']

        return Trello.boards.get(self.trello_board_id)

    ########################################################################################

    def __str__(self):
        return self.fqdn or self.name

    def save(self, *args, **kwargs):
        if not len(self.fqdn):
            self.fqdn = '%(name)s.fog.myopla.net' % self.__dict__

        for k in ('login', 'passwd'):
            if not self.__dict__['api_'+k]:
                setattr(self, 'api_'+k, uuid.uuid4().hex)

        return super(HostNode, self).save(*args, **kwargs)

class HostSSH(models.Model):
    hostnode   = models.ForeignKey(HostNode, related_name='ssh_keys')
    username   = models.CharField(max_length=64)

    pub_key    = models.TextField(blank=True)
    priv_key   = models.TextField(blank=True)

class HostCommand(models.Model):
    hostnode   = models.ForeignKey(HostNode, related_name='commands')
    command    = models.CharField(max_length=512)

    when       = models.DateTimeField(auto_now_add=True)
    stream     = models.CharField(max_length=32)
    content    = models.TextField(blank=True)
