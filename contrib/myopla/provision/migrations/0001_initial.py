# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ci', '0001_initial'),
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='HostCommand',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('command', models.CharField(max_length=512)),
                ('when', models.DateTimeField(auto_now_add=True)),
                ('stream', models.CharField(max_length=32)),
                ('content', models.TextField(blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HostNode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('fqdn', models.CharField(max_length=64, blank=True)),
                ('ip_addr4', models.GenericIPAddressField(default=None, null=True, verbose_name=b'IPv4', blank=True)),
                ('ip_addr6', models.GenericIPAddressField(default=None, null=True, verbose_name=b'IPv6', blank=True)),
                ('api_login', models.CharField(max_length=64, blank=True)),
                ('api_passwd', models.CharField(max_length=64, blank=True)),
                ('hipchat_room_id', models.PositiveIntegerField(default=None, null=True, blank=True)),
                ('trello_board_id', models.PositiveIntegerField(default=None, null=True, blank=True)),
                ('owner', models.ForeignKey(related_name='nodes', default=None, blank=True, to='core.Profile', null=True)),
                ('project', models.ForeignKey(related_name='nodes', default=None, blank=True, to='ci.Project', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HostSSH',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=64)),
                ('pub_key', models.TextField(blank=True)),
                ('priv_key', models.TextField(blank=True)),
                ('hostnode', models.ForeignKey(related_name='ssh_keys', to='provision.HostNode')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='hostcommand',
            name='hostnode',
            field=models.ForeignKey(related_name='commands', to='provision.HostNode'),
            preserve_default=True,
        ),
    ]
