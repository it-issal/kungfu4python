from django import template

register = template.Library()

import time

def timestamp(value):
    return time.asctime(time.gmtime(value))

register.filter(timestamp)
