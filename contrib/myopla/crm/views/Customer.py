#-*- coding: utf-8 -*-

from myopla.shortcuts import *

from myopla.crm.models import *

##############################################################################

@login_required
def listing(request):
    return render(request, 'rest/customer/list.html', dict(
        listing = Customer.objects.order_by('codename'),
    ))

@login_required
def overview(request, narrow):
    obj = Customer.objects.get(codename=narrow)

    if obj:
        return render(request, 'rest/customer/view.html', dict(
            customer = obj,
        ))
    else:
        return render(request, 'not-found.html', dict(
            narrow = narrow,
        ))
