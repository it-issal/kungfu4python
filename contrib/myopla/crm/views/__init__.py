#-*- coding: utf-8 -*-

from myopla.shortcuts import *

from myopla.crm.models import *

##############################################################################

@login_required
def homepage(request):
    return render(request, 'pages/homepage.html', dict(

    ))

##############################################################################

@login_required
def upcoming_events(request, lookup='all'):
    return render(request, 'pages/upcoming-events.html', dict(

    ))

##############################################################################

from . import Customer
