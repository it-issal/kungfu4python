# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('region', models.CharField(max_length=512, blank=True)),
                ('country', models.CharField(max_length=64)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('codename', models.CharField(max_length=128)),
                ('email', models.EmailField(max_length=75)),
                ('brand_name', models.CharField(max_length=256, blank=True)),
                ('summary', models.CharField(max_length=512, blank=True)),
                ('address', models.TextField(blank=True)),
                ('chief_design', models.ForeignKey(related_name='designs', to='core.Profile')),
                ('chief_technical', models.ForeignKey(related_name='projects', to='core.Profile')),
                ('city', models.ForeignKey(related_name='customers', blank=True, to='crm.City')),
                ('leader', models.ForeignKey(related_name='customer', to='core.Profile')),
                ('sales_team', models.ManyToManyField(related_name='sales', to='core.Profile', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TrackerComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message', models.TextField()),
                ('author', models.ForeignKey(related_name='comments', to='core.Profile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TrackerIssue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=512)),
                ('content', models.TextField()),
                ('level', models.CharField(default=b'major', max_length=24, choices=[(b'trivial', b'Trivial'), (b'minor', b'Minor'), (b'major', b'Major'), (b'critical', b'Critical'), (b'blocker', b'Blocker')])),
                ('nature', models.CharField(default=b'bug', max_length=24, choices=[(b'bug', b'Bug'), (b'proposal', b'Proposal'), (b'enhancement', b'Enchancement'), (b'hardware', b'Hardware malfunction')])),
                ('assignee', models.ForeignKey(related_name='assigned_issues', default=None, blank=True, to='core.Profile', null=True)),
                ('customer', models.ForeignKey(related_name='issues', to='crm.Customer')),
                ('reporter', models.ForeignKey(related_name='reported_issues', to='core.Profile')),
                ('watchers', models.ManyToManyField(related_name='watched_issues', to='core.Profile', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='trackercomment',
            name='issue',
            field=models.ForeignKey(related_name='comments', to='crm.TrackerIssue'),
            preserve_default=True,
        ),
    ]
