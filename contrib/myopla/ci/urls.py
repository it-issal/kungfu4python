from django.conf.urls import patterns, include, url

urlpatterns = patterns('myopla.ci.views',
    url(r'^templates/$',                                        'Template.listing',          name='template_home'),
    url(r'^templates/(?P<narrow>.+)/$',                         'Template.overview',         name='template_overview'),

    url(r'^projects/$',                                         'Project.listing',           name='projects_home'),
    url(r'^projects/(?P<narrow>.+)/$',                          'Project.overview',          name='projects_overview'),
    url(r'^projects/(?P<narrow>.+)/scenario$',                  'Project.scenario',          name='projects_scenario'),
    url(r'^projects/(?P<narrow>.+)/repository$',                'Project.repository',        name='projects_repository'),
    url(r'^projects/(?P<narrow>.+)/repository/(?P<hexsha>.+)$', 'Project.repository_commit', name='projects_commit'),

    url(r'^builds/$',                                           'Builder.listing',           name='builds_home'),
    url(r'^builds/(?P<narrow>.+)/$',                            'Builder.overview',          name='builds_overview'),

    url(r'^hook$',                                              'webhook',                   name='ci_webhook'),

    url(r'^planning$',                                          'plannified',                name='ci_upcoming'),
    url(r'^$',                                                  'homepage',                  name='homepage_ci'),
)
