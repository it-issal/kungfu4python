from myopla.shortcuts import *

registry = []

class Flavor(object):
    @classmethod
    def register(cls, hnd):
        registry.append(hnd)

        return hnd

    @classmethod
    def detect(cls, prj):
        for dtp in registry:
            inst = dtp(prj)

            if inst.detect():
                inst.invoke('prepare')

                return inst

        return None

    def __init__(self, prj):
        self._prj = prj

        self.invoke('initialize')

    def invoke(self, method, *args, **kwargs):
        hnd = getattr(self, method, None)

        if callable(hnd):
            return hnd(*args, **kwargs)
        else:
            return None

    name    = property(lambda self: self.__class__.__name__)
    project = property(lambda self: self._prj)

    def activate(self):
        self.invoke('build')

        self.invoke('install')

        self.invoke('deploy')

    def deactivate(self):
        self.invoke('remove')

        self.invoke('clean')

    def rpath(self, *path):
        return os.path.join(self.project.git_path, *path)

    def exists(self, *path):
        return os.path.exists(self.rpath(*path))

    def shell(self, *cmd):
        def clean_shell(x):
            if ' ' in x:
                x = '"' + x + '"'

            return x

        os.system(' '.join([
            clean_shell(x)
            for x in cmd
        ]))

    def __str__(self):     return str(getattr(self, 'TITLE', self.name))
    def __unicode__(self): return unicode(getattr(self, 'TITLE', self.name))

################################################################################

@Flavor.register
class PHP(Flavor):
    TITLE = "PHP (+Composer)"

    def detect(self):
        return self.exists('index.php')

    def install(self):
        if self.exists('index.php'):
            self.shell('composer', 'install')

    def deploy(self):
        self.shell('chown', 'www-data:www-data', '-R', self.rpathg('.'))

################################################################################

@Flavor.register
class Python(Flavor):
    TITLE = "Python"

    def detect(self):
        return self.exists('requirements.txt')

    def initialize(self):
        pass

    def prepare(self):
        pass

    def build(self):
        if not self.exists('venv'):
            self.shell('virtualenv', '--distribute', 'venv')

    def install(self):
        if self.exists('venv'):
            self.shell('venv/bin/pip', 'install', '-r', 'requirements.txt')

    def deploy(self):
        if self.exists('setup.py'):
            self.shell('venv/bin/python', 'setup.py', 'install')

        if self.exists('manage.py'):
            self.shell('venv/bin/python', 'manage.py', 'collectstatic', '-l', '--no-input')

################################################################################

@Flavor.register
class JavaAnt(Flavor):
    TITLE = "Java Ant"

    def detect(self):
        return self.exists('pom.xml')

    def build(self):
        self.shell('ant', 'debug')

    def install(self):
        self.shell('ant', 'release')

    def clean(self):
        self.shell('ant', 'clean')

@Flavor.register
class JavaMaven(Flavor):
    TITLE = "Java Maven"

    def detect(self):
        return self.exists('pom.xml')

    def build(self):
        self.shell('maven', 'package')

    def install(self):
        self.shell('maven', 'install')

    def clean(self):
        self.shell('maven', 'clean')
