#-*- coding: utf-8 -*-

from myopla.shortcuts import *

from myopla.ci.models import *

##############################################################################

@login_required
def listing(request):
    return render(request, 'rest/project/list.html', dict(
        listing = Project.objects.order_by('name'),
    ))

@login_required
def overview(request, narrow):
    obj = Project.objects.get(name=narrow)

    if obj:
        return render(request, 'rest/project/view.html', dict(
            project = obj,
        ))
    else:
        return render(request, 'not-found.html', dict(
            narrow = narrow,
        ))

@login_required
def scenario(request, narrow):
    obj = Project.objects.get(name=narrow)

    if obj:
        return render(request, 'rest/project/scenario.html', dict(
            project = obj,
        ))
    else:
        return render(request, 'not-found.html', dict(
            narrow = narrow,
        ))

@login_required
def repository(request, narrow):
    obj = Project.objects.get(name=narrow)

    if obj:
        return render(request, 'rest/project/repository.html', dict(
            project = obj,
            listing = obj.commits(),
        ))
    else:
        return render(request, 'not-found.html', dict(
            narrow = narrow,
        ))

@login_required
def repository_commit(request, narrow, hexsha):
    obj = Project.objects.get(name=narrow)

    if obj:
        lst = []

        try:
            lst += obj.git_repo.iter_commits('master', max_count=200)
        except:
            return render(request, 'not-found.html', dict(
                narrow = narrow,
            ))

        return render(request, 'rest/project/repository.html', dict(
            project = obj,
            listing = lst,
        ))
    else:
        return render(request, 'not-found.html', dict(
            narrow = narrow,
        ))
