#-*- coding: utf-8 -*-

from voodoo.shortcuts import *

#########################################################################################

class vHostCommand(PersistentDirective):
    repo = property(lambda self: self.parent.repo)
    args = property(lambda self: self.parent.args)

    #************************************************************************************

    def rpath(self, *pth):
        return self.parent.rpath(*pth)

    def resolve(self, *args, **kwargs):
        return self.parent.resolve(*args, **kwargs)

#########################################################################################

class vHostDirective(vHostCommand):
    def configure(self):
        self.parser.add_argument('-pl', '--pull', dest='pull', action='store_true',
                            help="Pull before.")

        self.parser.add_argument('-ps', '--push', dest='push', action='store_true',
                            help="Push after.")

        self.parser.add_argument('-d', '--default', dest='default_remote', action='store_true',
                            help="Include default remote in operations.")

        self.parser.add_argument('-r', '--remote', dest='remotes', type=str, action='append',
                            default=['origin'], help="Remote to use.")

        if hasattr(self, 'extend'):
            if callable(self.extend):
                self.extend()

#########################################################################################

class HostingDirective(vHostCommand):
    def configure(self):
        if hasattr(self, 'extend'):
            if callable(self.extend):
                self.extend()

#########################################################################################

class DemingDirective(vHostCommand):
    def configure(self):
        self.parser.add_argument('-pl', '--pull', dest='pull', action='store_true',
                            help="Pull before.")

        self.parser.add_argument('-ps', '--push', dest='push', action='store_true',
                            help="Push after.")

        self.parser.add_argument('-d', '--default', dest='default_remote', action='store_true',
                            help="Include default remote in operations.")

        self.parser.add_argument('-r', '--remote', dest='remotes', type=str, action='append',
                            default=['origin'], help="Remote to use.")

        if hasattr(self, 'extend'):
            if callable(self.extend):
                self.extend()

    #************************************************************************************

    def do_pull(self, *args, **kwargs):
        #for rmt in self.repo.remotes:
        #    if True or rmt in self.args['remotes']:
        for rmt in self.args['remotes']:
                self.repo.pull(self.args['branch'], rmt)

    def do_push(self, *args, **kwargs):
        #for rmt in self.repo.remotes:
        #    if rmt in self.args['remotes']:
        for rmt in self.args['remotes']:
            self.repo.push(self.args['branch'], rmt)

#########################################################################################

class vHostingTool(Commandline):
    #TITLE = "vHostCTL : vHosting management tool."
    CFG_PATH = '/hml/etc/charms/voodoo.yml'

    #************************************************************************************

    def prepare(self, *args, **kwargs):
        self.cfg = Nucleon.local.read_yaml(self.CFG_PATH)

        self.cfg = list(self.cfg)

        self.bootstrap(*args, **kwargs)

        #for svc in self.daemons:
        #    svc.config_daemon(

        #    )

    #************************************************************************************

    def terminate(self, *args, **kwargs):
        self.cleanup(*args, **kwargs)

        self.cfg = [x.__soul__ for x in Nucleon.vhosts.values()]
        Nucleon.local.write_yaml(self.CFG_PATH, self.cfg)

    #************************************************************************************

    daemons = property(lambda self: [
        svc
        for svc in Nucleon.daemons.values()
        if svc.name in ['nginx', 'apache2', 'supervisor']
    ])

    #************************************************************************************

    def resolve(self, *keys):
        return sorted([
            vht
            for vht in Nucleon.vhosts.values()
            if len(keys)==0 or (vht.narrow in keys)
        ], key=lambda vht: vht.narrow)

        return resp

    REPO_FLAGs = (
        ('X', "enabled", "disabled", lambda vht: vht.status),
        ('D', "dirty",   "clean",    lambda vht: vht.repo.dirty),
        ('S', "stale",   "synced",   lambda vht: vht.repo.stale),
        ('B', "bare",    "setup",    lambda vht: vht.repo.stale),
    )

    def badge_vhost(self, vht, level=0):
        prefix = "\t" * level

        print prefix+"-> narrow  : %s" % vht.narrow
        print prefix+"   fqdn    : %s" % vht.fqdn
        print prefix+"   path    : %s" % vht.bpath

        if vht.repo.flavor or True:  print prefix+"   flavor  : %s" % vht.repo.flavor
        if len(vht.domains): print prefix+"   domains : %s" % ', '.join(vht.domains)
        if len(vht.aliases): print prefix+"   aliases : %s" % ', '.join(vht.aliases)

        if len(vht.repo_links):
            print prefix+"   remotes :"

            for alias,link in vht.repo_links.iteritems():
                print prefix+"        --> %s \t: %s" % (alias,link)

        print prefix+"   flags   : " + ', '.join([
            iif(cb(vht), tlabel, flabel)
            for key,tlabel,flabel,cb in self.REPO_FLAGs
        ])

    ###################################################################################

    #@reflect(lambda cmd: cmd.add_flag('all', '-a', '--all', help="Host to operate on."))
    #@reflect(lambda cmd: cmd.add_param('dirs', '-d', '--dirs', help="Dirs to upgrade."))
    #@Commandline.sub('upgrade')
