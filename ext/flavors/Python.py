#-*- coding: utf-8 -*-

from voodoo.shortcuts import *

class Python(Flavor):
    def environment(self):
        if not self.exists('venv'):
            self.shell('virtualenv', '--distribute', 'venv/')
            
            self.shell('venv/bin/easy_install', '-U', 'distribute')
        
        self.shell('venv/bin/pip', 'install', '-r', 'requirements.txt')

@Flavor.register
class WSGI(Python):
    def detect(self):
        return self.exists('main.wsgi')
    
    def config_nginx(self, svc):
        yield '    location / {'
        yield '        proxy_pass http://localhost:%(http)d;' % svc.PORTs['apache2']
        yield '        include /etc/nginx/proxy_params;'
        yield '    }'
    
    def config_apache2(self, svc):
        yield '	    WSGIScriptAlias / %(bpath)s/main.wsgi' % self.resource
        yield '	    WSGIDaemonProcess horizon user=%(owner)s group=%(owner)s processes=3 threads=10 home=%(bpath)s' % self.target
        yield '	    WSGIApplicationGroup %{GLOBAL}'
        
        yield '	    WSGIProcessGroup %(owner)s-%(name)s'
        yield '	    SetEnv APACHE_RUN_USER %(owner)s' % self.resource
        yield '	    SetEnv APACHE_RUN_GROUP %(owner)s' % self.resource
        
        yield '    <Directory "%(bpath)s/">' % self.resource
        yield '        Options Indexes FollowSymLinks MultiViews'
        yield '        AllowOverride All'
        yield '        Order allow,deny'
        yield '        Allow from all'
        yield '        Require all granted'
        yield '        Satisfy Any'
        yield '    </Directory>'

@Flavor.register
class Django(Python):
    def detect(self):
        return self.exists('manage.py')
    
    def config_nginx(self, svc):
        yield '    location / {'
        yield '        proxy_pass http://localhost:%d;' % self.resource.get_port('http')
        yield '        include /etc/nginx/proxy_params;'
        yield '    }'
    
    def config_supervisor(self, svc):
        yield '[program:vhost-%(owner)s-%(name)s]' % self.resource
        yield 'command     = venv/bin/python manage.py runserver --noreload 127.0.0.1:%d' % self.resource.get_port('http')
        yield 'directory   = %(bpath)s' % self.resource
        yield 'autostart   = true'
        yield 'autorestart = true'
        #yield 'user        = %(owner)s' % self.resource
