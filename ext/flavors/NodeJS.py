#-*- coding: utf-8 -*-

from voodoo.shortcuts import *

@Flavor.register
class NodeJS(Flavor):
    def detect(self):
        return self.exists('package.json')
    
    def environment(self):
        self.shell('npm', 'install')
    
    def config_nginx(self, svc):
        yield '    location / {'
        yield '        proxy_pass http://localhost:%d;' % self.resource.get_port('http')
        yield '        include /etc/nginx/proxy_params;'
        yield '    }'
    
    def config_supervisor(self, svc):
        yield '[vhost-%(owner)s:%(name)s]' % self.resource
        yield 'command     = nodejs main.js'
        yield 'environment = PORT=%d' % self.resource.get_port('http')
        yield 'directory   = %(bpath)s' % self.resource
        yield 'autostart   = false'
        yield 'autorestart = true'
        yield 'user        = %(owner)s' % self.resource

