#-*- coding: utf-8 -*-

from voodoo.shortcuts import *

@Flavor.register
class PHP5(Flavor):
    def detect(self):
        return self.exists('.htaccess') or self.exists('index.php') or self.exists('composer.json')
        
        return 0 < len([
            pth
            for pth in [
                set(prn + ['index.%s' % ext])
                for prn in [
                    [],
                    ['www'],
                    ['www-data'],
                    ['ww-data'],
                ]
                for ext in ('htm','html','php')
            ] + [
                ('composer.json',),
            ]
            if self.exists(*pth)
        ])
    
    def environment(self):
        if self.exists('composer.json'):
            self.shell('composer', 'install')
            
            self.shell('composer', 'update')
        
        self.shell('chown', 'www-data:www-data', '-R', '.')
        self.shell('chmod', '775', '-R', '.')
        self.shell('chmod', '+s', '-R', '.')
    
    def config_nginx(self, svc):
        yield '    location / {'
        yield '        proxy_pass http://localhost:%(http)d;' % svc.PORTs['apache2']
        yield '        include /etc/nginx/proxy_params;'
        yield '    }'
    
    def config_apache2(self, svc):
        yield '	    DocumentRoot %(bpath)s' % self.resource
        
        yield '    <Directory "%(bpath)s/">' % self.resource
        yield '        Options Indexes FollowSymLinks MultiViews'
        yield '        AllowOverride All'
        yield '        Order allow,deny'
        yield '        Allow from all'
        yield '        Require all granted'
        yield '        Satisfy Any'
        yield '    </Directory>'
        
        yield '	    <IfModule mod_php5.c>'
        yield '		    php_value display_errors On'
        yield '		    #php_admin_flag engine on'
        yield '	    </IfModule>'

