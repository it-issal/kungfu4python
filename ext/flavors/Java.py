#-*- coding: utf-8 -*-

from voodoo.shortcuts import *

@Flavor.register
class Grails(Flavor):
    def detect(self):
        return self.exists('application.properties') and self.exists('grails-app')
    
    def environment(self):
        self.shell('mvn', 'install')
    
    def config_nginx(self, svc):
        yield '    location / {'
        yield '        proxy_pass http://localhost:%d;' % self.resource.get_port('http')
        yield '        include /etc/nginx/proxy_params;'
        yield '    }'
    
    def config_supervisor(self, svc):
        yield '[vhost-%(owner)s:%(name)s]' % self.resource
        yield 'command     = nodejs main.js'
        yield 'environment = PORT=%d' % self.resource.get_port('http')
        yield 'directory   = %(bpath)s' % self.resource
        yield 'autostart   = false'
        yield 'autorestart = true'
        yield 'user        = %(owner)s' % self.resource

@Flavor.register
class Maven(Flavor):
    def detect(self):
        return self.exists('pom.xml')
    
    def environment(self):
        self.shell('mvn', 'install')

@Flavor.register
class Android(Flavor):
    def detect(self):
        return self.exists('AndroidManifest.xml')
    
    def environment(self):
        self.shell('ant', 'debug')
        self.shell('ant', 'release')
        
        self.shell('ant', 'instrument')
        
        self.shell('ant', 'test')
        
        if self.exists(self.rpath('ci.env')):
            #source ci.env
            
            DEVICE = "apk-tester"
            #APK    = '$(ls bin/*.apk | tail -n 1)'
            
            self.shell('adb', '-s', DEVICE, 'install', APK)

