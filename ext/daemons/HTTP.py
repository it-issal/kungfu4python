#-*- coding: utf-8 -*-

from voodoo.shortcuts import *

@Daemon.register
class Nginx(ClusterDaemon):
    CFG_DEPLOY = property(lambda self: '/etc/nginx/sites-enabled')
    CFG_TARGET = property(lambda self: '/etc/nginx/sites-available')
    CFG_REGEX = re.compile('vhost-(.+)-(.+)\.conf')
    CFG_LOCATION = 'vhost-%(owner)s-%(name)s.conf'
    
    def config_daemon(self, **cnt):
        cnt.update(self.context)
        
        self.render('daemon.conf', '/etc/nginx/nginx.conf',
            daemons = self.PORTs,
        **cnt)
        
        for k in ('proxy','fastcgi','scgi','uwsgi'):
            self.render('params/%s' % k, '/etc/nginx/%s_params' % k,
                
            **cnt)
        
        Nucleon.local.write('/etc/nginx/mime.types', *[
            "types {",
        ] + [
            #open('%s/cluster/nginx/mimes/%s' % (Nucleon.TEMPLATES_ROOT, k)).read()
            Nucleon.local.read_tpl('cluster/nginx/mimes/%s' % k)
            for k in ('base','extra')
        ] + [
            "}",
        ])

@Daemon.register
class Apache2(ClusterDaemon):
    CFG_DEPLOY = property(lambda self: '/etc/%s/sites-enabled' % self.name)
    CFG_TARGET = property(lambda self: '/etc/%s/sites-available' % self.name)
    CFG_REGEX = re.compile('vhost-(.+)-(.+)\.conf')
    CFG_LOCATION = 'vhost-%(owner)s-%(name)s.conf'
    
    def config_daemon(self, **cnt):
        cnt.update(self.context)
        
        self.render('daemon.conf', '/etc/apache2/apache2.conf',
            
        **cnt)
        
        self.render('php.ini', '/etc/php5/apache2/php.ini',
            php5_path = [
                pth for pth in (
                    os.environ.get('PHP5_PATH', ''),
                    '.',
                    '/usr/share/php',
                ) if pth
            ],
        **cnt)

