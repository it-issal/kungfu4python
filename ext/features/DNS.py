#-*- coding: utf-8 -*-

from voodoo.shortcuts import *

@Feature.register
class DNS(Feature):
    def dns_entries(self, rfl):
        for entry in rfl.terminal('srv-api'):
            yield entry
        
        yield rfl.CNAME('api',    'srv-api')
        yield rfl.CNAME('data',   'srv-api')
        yield rfl.CNAME('status', 'srv-api')

