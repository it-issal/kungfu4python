#-*- coding: utf-8 -*-

from voodoo.shortcuts import *

@Feature.register
class CDN(Feature):
    def dns_entries(self, rfl):
        for entry in rfl.terminal('srv-cdn'):
            yield entry
        
        yield rfl.CNAME('assets', 'srv-cdn')
        yield rfl.CNAME('media',  'srv-cdn')

