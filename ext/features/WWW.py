#-*- coding: utf-8 -*-

from voodoo.shortcuts import *

@Feature.register
class WWW(Feature):
    def dns_entries(self, rfl):
        for entry in rfl.terminal('srv-www'):
            yield entry
        
        yield rfl.CNAME('ipv6',  'srv-www')
        yield rfl.CNAME('www',  'srv-www')

