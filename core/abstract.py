#-*- coding: utf-8 -*-

from python2use.shortcuts import *
from deming.shortcuts     import *

class vDomain(object):
    def __init__(self, vht, fqdn, features=['www','cdn','']):
        self._vht   = vht
        self._dns  = fqdn
        self._fts  = features

    vhost    = property(lambda self: self._vht)
    fqdn     = property(lambda self: self._dns)
    features = property(lambda self: self._fts)

class vHost(Manageable):
    TPL_NARROW = '%(owner)s/%(name)s'
    TPL_PURPLE = [
        '%(name)s.vhost.%(owner)s.zone',
        '%(name)s.%(owner)s.maher.zone',
    ]

    SUBDIR_NARROW = 'vhosts'

    CONTEXT_CORE  = ('domains', 'aliases', 'features', 'bpath', 'deploy', 'repo_links', 'gateway')
    CONTEXT_EXTRA = ('fqdn', 'bpath', 'commandline', 'ctl_path', 'cfg_path')

    def __init__(self, mgr, bpath, owner, name, features=['www','cdn'], domains=[], aliases=[], deploy=None, repo_links={}, gateway={'type': 'local'}):
        super(vHost, self).__init__(mgr, bpath, owner, name)

        self._fts   = features
        self._dns   = domains
        self._fqdns = aliases
        self._ship  = deploy
        self._rmts  = repo_links
        self._gw    = gateway

    fqdn    = property(lambda self: self.TPL_PURPLE[0] % dict(owner=self.owner, name=self.name))
    fqdn2   = property(lambda self: self.TPL_PURPLE[1] % dict(owner=self.owner, name=self.name))

    features = property(lambda self: self._fts)
    deploy   = property(lambda self: self._ship)

    meta     = property(lambda self: self)
    gateway  = property(lambda self: self._gw)

    backends = property(lambda self: [])

    @property
    def env(self):
        cnt = {}

        for bkn in self.backends:
            for key in bkn.env:
                cnt[key] = bkn.env[key]

        cnt['APP_OWNER'] = self.owner
        cnt['APP_NAME']  = self.name

        cnt['FQDN'] = self.fqdn
        cnt['PORT'] = self.get_port('http')

        return cnt

    submodules = property(lambda self: self.repo.submodules)

    @property
    def repo_links(self):
        return self.repo.remote_links

    @property
    def components(self):
        return [
            Nucleon.features[key]
            for key in self.features
            if key in Nucleon.features
        ]

    def persist(self):
        Nucleon.cache.set('vhost://%s/domains' % self.narrow, self.domains)
        Nucleon.cache.set('vhost://%s/fqdns' % self.narrow, self.fqdns)

        #print "\t#) Configuring vhost : %(narrow)s" % self

        for svc in self.manager.daemons:
            if hasattr(svc, 'config_domain'):
                for d in self.domains:
                    try:
                        svc.config_domain(Reflector(self, d))
                    except Exception,ex:
                        print "Failed configuring domain '%s' for service '%s' !" % (d, svc)
                        print ex

            if hasattr(svc, 'config_vhost'):
                #print "\t\t -> %s ..." % svc.name

                try:
                    svc.config_vhost(self)
                except Exception,ex:
                    print "Failed configuring vhost '%s' for service '%s' !" % (self, svc)
                    print ex

    @property
    def status(self):
        rst = [
            svc
            for svc in self.manager.daemons
            if svc.status_vhost(self)
        ]

        if len(rst):
            self.enable()

            return True

        return False

    def enable(self):
        for svc in self.manager.daemons:
            svc.enable_vhost(self)

    def disable(self):
        for svc in self.manager.daemons:
            svc.disable_vhost(self)

    @property
    def repo(self):
        if not hasattr(self, '_repo'):
            setattr(self, '_repo', Repository(self.bpath, parent=self))

        return getattr(self, '_repo', None)

    domains = property(lambda self: self._dns)
    aliases = property(lambda self: self._fqdns)

    @property
    def fqdns(self):
        return [
            self.TPL_PURPLE[1] % self,
        ] + [
            '.'.join(list(alias)+[d])
            for alias in (
                [],
                ['ipv6'],
                ['www'],
            )
            for d in self.domains
        ] + [ fqdn for fqdn in self.aliases ]

class Reflector(object):
    def __init__(self, vhost, fqdn):
        self._vhost = vhost
        self._fqdn = fqdn

    vhost   = property(lambda self: self._vhost)
    fqdn    = property(lambda self: self._fqdn)

    manager = property(lambda self: self.vhost.manager)
    daemons = property(lambda self: self.manager.daemons)

    ipv4    = property(lambda self: [
        '37.187.49.38',
    ])

    ipv6    = property(lambda self: [
        '2001:41d0:52:400::455',
    ] + [
        '2001:470:71:5ef::%d:1' % i
        for i in range(1,7)
    ])

    @property
    def nameservers(self):
        yield 'ns1', 'NS', 'ns1.maher-ops.pl.'

        yield 'ns2', 'NS', 'ns2.maher-ops.pl.'

    def rqdn(self, *parts):
        return '.'.join(list(parts)+[self.fqdn])

    def terminal(self, alias):
        for ep in self.ipv4:
            yield self.A(alias, ep)

        for ep in self.ipv6:
            yield self.AAAA(alias, ep)

    def NS(self, alias, target):
        return alias, 'NS', target

    def A(self, alias, target):
        return alias, 'A', target

    def AAAA(self, alias, target):
        return alias, 'AAAA', target

    def CNAME(self, alias, target):
        return alias, 'CNAME', target

    @property
    def entries(self):
        for a,t,v in self.nameservers:
            yield '@', 'NS', self.rqdn(a)

        yield '@', 'A', self.rqdn(a)

        for ft in self.vhost.components:
            for entry in ft.dns_entries(self):
                yield entry
