#-*- coding: utf-8 -*-

#from python2use.core.meta import *

from python2use.shortcuts import *

from shelter.utils import *

class Feature(object):
    @classmethod
    def register(cls, hnd):
        if hasattr(hnd, '__name__'):
            ft = hnd()
            
            if ft.name not in Nucleon.features:
                Nucleon.features[ft.name] = ft
        
        return hnd
    
    def __init__(self):
        super(Feature, self).__init__()
    
    name     = property(lambda self: self.__class__.__name__.lower())
    
    def daemon_config(self, svc):
        resp = []
        
        hnd = getattr(self, 'config_%s' % svc.name, None)
        
        if callable(hnd):
            resp = [ln for ln in hnd(svc)]
        
        return "\n".join(resp)
    
    def __str__(self):     return str(self.name)
    def __unicode__(self): return unicode(self.name)
    
class Flavor(object):
    @classmethod
    def register(cls, hnd):
        if hasattr(hnd, '__name__'):
            if hnd not in Nucleon.flavors:
                Nucleon.flavors.append(hnd)
        
        return hnd
    
    @classmethod
    def provision(cls, target):
        #print Nucleon.flavors
        
        for flv in Nucleon.flavors:
            #print ("#) Checking '%s' for flavor '%s' ..." % (target.rpath('.'), flv.name))
            
            wrp = flv(target)
            
            if wrp.detect():
                return wrp
            else:
                pass#print ("#) '%s' is not a %s flavor ..." % (target.rpath('.'), flv.name))
        
        return None
    
    def __init__(self, target):
        super(Flavor, self).__init__()
        
        self._trg = target
    
    name     = property(lambda self: self.__class__.__name__.lower())
    tpl_root = property(lambda self: '/'.join(['features',self.name]))
    
    target   = property(lambda self: self._trg)
    resource = property(lambda self: self.target.meta)
    
    def exists(self, *args, **kwargs): return self.target.exists(*args, **kwargs)
    def chdir(self, *args, **kwargs):  return self.target.chdir(*args, **kwargs)
    def shell(self, *args, **kwargs):  return self.target.shell(*args, **kwargs)
    
    def control(self, acte):
        pass
    
    def __str__(self):     return str(self.name)
    def __unicode__(self): return unicode(self.name)

